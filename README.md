# API Calculo Boleta
Realiza el cálculo de Boletas ya sea de IVA u Honorarios, con ingreso del tipo y monto, retornando un json con los cálculos ya sea bruto o liquido, retención o impuesto, de la boleta en cuestión.

## Preliminares
- Puede obtener el código con ```git clone git@gitlab.com:ra_cl/api_calculo_boleta.git```
- Existe una imagen docker basada en la última version estable del código original que puede obtener haciendo:
  
  ```bash
  docker pull twkralvear/api_calculo_boleta:latest
  ```

- Se ha dejado un Dockerfile para el código tal cual está en este momento, puede ejecutar en su máquina para generar una imagen con:

  ```bash
  docker build -t api_calculo_boleta .
  docker run -d --name api -e TOKEN=secret --rm api_calculo_boleta
  ```

## Requerimientos
- PHP version estable
- Nginx o Apache

## Configuraciones
- Acceso a las variables de entorno
- Definir las variables de entorno

## Variables de Entorno
|Variable|Descripción|Valor de ejemplo|
|---|---|---|
|TOKEN|token que permite la autorización a la ejecución de la API|secret|

## Test de configuración
- invoque la URL, debería conseguir un json con un mensaje de error.
- agruegue los parámetros "impuesto", "valor" y "token" vía GET, con lo que debería obtener el json con el resultado.
  
  Ejemplo:
  ```bash
  curl -i -L "http://url_api.twk?valor=60000&impuesto=iva&token=secret"
  ```