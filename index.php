<?php

class CalculaImpuestos
{
    private $token = false;
    private $parametros = array();
    private $retencion;
    private $estado ='';

    public function __construct ()
    {
        $this->token = getenv('TOKEN');
        $this->retencion = $this->get_retencion();
    }

    private function get_retencion ()
    {
        $this->retencion = parse_ini_file("valores.ini", true);
        return $this->retencion;
    }

    private function autoriza ($token)
    {
        return ($this->token == $token);
    }

    private function ok_parameters ($array, $token)
    {
        $ok = true;
        $ok = $ok && (isset($array['valor']) && is_int($array['valor']));
        $ok = $ok && (isset($array['impuesto']) && isset($this->retencion[$array['impuesto']]));
        $this->parametros = $ok;
        $this->estado = $this->status($array, $token);
        return $ok;
    }

    public function status ($par, $token)
    {
        $msg = '';
        if (getenv('TOKEN') === false) $msg .= "TOKEN NO DEFINIDO<br>\n";
        elseif (getenv('TOKEN') != $token) $msg .= "Token no autorizado<br>\n";
        if ($this->parametros == false) $msg .= "Parametros mal ingresados<br>\n";
        if (!isset($par['impuesto'])) $msg.="impuesto no definido<br>\n";
        elseif (isset($par['impuesto']) && !isset($this->retencion[$par['impuesto']])) $msg .= "Retención o impuesto mal ingresado<br>\n";
        if (!isset($par['valor'])) $msg.="valor no definido<br>\n";
        if ($msg == '') {
            $msg .= "Todo Ok";
        }
        return $msg;
    }

    public function calcula ($parametros, $token)
    {
        $impuestoReal = 0;
        $caso1 = $caso2 = array();
        if ($this->ok_parameters($parametros, $token) && $this->autoriza($token)) {
            include_once('impuesto.php');
            $imp = new Impuesto();
            $parametros['impuesto'] = $this->retencion[$parametros['impuesto']];
            $caso1 = $imp->calcula_total($parametros, $token);
            $caso2 = $imp->calclula_liquido($parametros, $token);
            $resp = array(
                'bruto' => $caso1,
                'liquido' => $caso2,
                'estado' => $this->estado,
                'exito' => ($caso1['exito'] + $caso2['exito'])==2?1:0,
            );
        } else {
            $resp = array(
                'estado' => $this->estado,
                'exito' => 0,
            );
        }
        return $resp;
    }
}

function main()
{
    $tkn = '';
    if (isset($_GET['token'])) {
        $tkn = $_GET['token'];
        unset($_GET['token']);
    }
    if (isset($_GET['valor']) && is_numeric($_GET['valor'])) {
        $_GET['valor'] = 0 + $_GET['valor'];
    }
    //$tkn = 123;
    //putenv("TOKEN=$tkn");
    //$_GET = array('impuesto' => 'iva', 'valor'=>50000);
    $i = new CalculaImpuestos();
    $r = $i->calcula($_GET, $tkn);
    header('Content-Type: application/json');
    print_r(json_encode($r));
}

main();
