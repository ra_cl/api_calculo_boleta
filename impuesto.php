<?php

class Impuesto
{
    private $parametros = false;
    private $exito = false;
    private $status = '';

    private function ok_parameters ($array)
    {
        $ok = true;
        $ok = $ok && (isset($array['valor']) && is_int($array['valor']));
        $ok = $ok && (isset($array['impuesto']) && is_numeric($array['impuesto']));
        $this->status = $this->status($array);
        $this->parametros = $ok;
        return $ok;
    }

    private function status ($param)
    {
        $msg = '';
        $p = '';
        foreach($param as $i => $v) $p .= "$i=$v; ";
        if ($this->parametros == false) $msg .= "Parametros mal ingresados $p<br>\n";
        if ($msg == '') {
            $msg .= "Todo Ok";
        }
        return $msg;
    }

    private function exito()
    {
        return ($this->exito?1:0);
    }

    public function calcula_total ($parametros, $token)
    {
        $valorTotal = 0;
        $retencion = 0;
        if ($this->ok_parameters($parametros)) {
            $valorTotal = $parametros['valor'] * (100 + $parametros['impuesto'])/100;
            $valorTotal = $this->aproxima($valorTotal);
            $retencion =$valorTotal -  $parametros['valor'];
            $this->exito = true;
        }
        $resp = array(
            'valor' => $valorTotal,
            'retencion' => $retencion,
            'estado' => $this->status($parametros),
            'exito' => $this->exito()
        );
        return $resp;
    }

    private function aproxima($valor)
    {
        $r = $valor + 0.5;
        return round($r, 0, PHP_ROUND_HALF_DOWN);
    }

    public function calclula_liquido ($parametros, $token)
    {
        $liquido = 0;
        $retencion = 0;
        if ($this->ok_parameters($parametros)) {
            $liquido = $parametros['valor'] * 100/(100 + $parametros['impuesto']);
            $liquido = $this->aproxima($liquido);
            $retencion = $parametros['valor'] - $liquido;
            $this->exito = true;
        }
        $resp = array(
            'valor' => $liquido,
            'retencion' => $retencion,
            'estado' => $this->status,
            'exito' => $this->exito()
        );
        return $resp;
    }   
}
